#About

Proof of concept for a small webservice to register android devices and send them notifications using node-gcm module and a sqlite database. Sample example of how to use the node-gcm module. Unregistering is not yet implemented.

#How to use it

Type "npm install" to install the dependencies automatically, except for sqlite3 (run "npm install sqlite3"). Run "node index.js" after having executed "node create_db.js" to have the correct database.

Change the "gcm_apikey" parameter in config.js file with your own GCM API key. You can also change the default listening port (default is 6999, so it doesn't interfere with my webserver). If you never used GCM before, just follow [those instructions](https://developer.android.com/google/gcm/gs.html) to get an API Key.

For the android part of the project, just use the [Google GCM Demo Application](https://developer.android.com/google/gcm/demo.html) or build your own using these informations:

 - URL to register: http://youserveraddress/register
 - POST parameter: regId

You can also check the form on the root of the webapp to see how to replicate it.

Once the server everything is set up, the server is running, launch the app on your android device and it should register itself to the server. Then call the URL http://youserveraddress/notify to send a notification to all devices register within the server.

#Credits

Most of the code "architecture" is inspired by the book ["Node Beginner"](http://nodebeginner.org/), this is using the node module [node-gcm](https://github.com/ToothlessGear/node-gcm/) (not reinventing the wheel here). The GCM documentation from Google was also very useful.
